import twist
import sys
import xbmcgui
import xbmcplugin

base_url = sys.argv[0]
addon_handle = int(sys.argv[1])
ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"

xbmcplugin.setContent(addon_handle, "tvshows")

mode = sys.argv[2]

if mode:
    mode = mode.strip("?")


def build_url(slug):
    return base_url + "?" + slug


anime_list = []
if not anime_list:
    anime_list = twist.get_anime_list()

if not mode:
    for anime in sorted(anime_list.iterkeys()):
        slug = anime
        title = anime_list[anime]["title"]
        url = build_url(slug)
        li = xbmcgui.ListItem(title)
        vid_details = {
            "mediatype": "tvshow",
            "dateadded": anime_list[anime]["created_at"],
        }
        if "alt_title" in anime_list[anime] and anime_list[anime]["alt_title"]:
            vid_details["originaltitle"] = anime_list[anime]["alt_title"]
        li.setInfo("video", vid_details)
        xbmcplugin.addDirectoryItem(
            handle=addon_handle, url=url, listitem=li, isFolder=True
        )

    xbmcplugin.endOfDirectory(addon_handle)
else:
    slug = mode
    details = twist.get_anime(slug)
    sources = twist.get_anime_sources(slug)
    counter = 0
    show_name = anime_list[slug]["title"]
    for ep in sources:
        ep_details = details["episodes"][counter]
        counter += 1
        url = ep + "|referer=https://twist.moe/a/{}/{}|user-agent: {}".format(
            slug, counter, ua
        )
        li = xbmcgui.ListItem("{} - Episode {}".format(show_name, counter))
        li.setInfo(
            "video",
            {
                "episode": counter,
                "mediatype": "episode",
                "genre": "anime",
                "tvshowtitle": show_name,
                "dateadded": ep_details["created_at"],
                "plot": details["description"],
            },
        )
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)
    xbmcplugin.endOfDirectory(addon_handle)
