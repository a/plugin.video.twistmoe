# plugin.video.twistmoe

Unofficial Kodi plugin for `twist.moe`, and a small library for getting data from it.

This is my first Kodi plugin, so some things might be broken.

It's confirmed to be working with Kodi 17 and 18. You just need `pycryptodome` installed with python2.

---

## Downloader

I also added a downloader script to the project for testing purposes.

You can go to twist.moe, pick an anime, and get the slug from URL. For example, for `https://twist.moe/a/hack-sign/1`, the slug is `hack-sign`.

You can then use it with `python3 dl.py sluggoeshere`. It will create a folder with the name as the slug, and download the videos as mp4s with filenames "1.mp4" etc, matching the episode number.

---

## How to install

- Enable third party add-ons in kodi settings
- Clone the repo (`git clone https://gitlab.com/a/plugin.video.twistmoe`)
- Enter the folder you just cloned (`cd plugin.video.twistmoe`)
- Install python2 requirements (`sudo -H pip2 install -Ur requirements.txt`)
- Leave the folder with the plugin (`cd ..`)
- Zip it (`zip -r plugin.video.twistmoe.zip plugin.video.twistmoe/`)
- Install that zip to kodi


Note: You can try downloading the zip directly from gitlab, but it'll append a `-master` at the end of the name, which might cause installation issues.

---

## Screenshots

![](https://awo.oooooooooooooo.ooo/i/g4a2bzr2.png)

![](resources/screenshot000.png)

![](resources/screenshot001.png)

![](resources/screenshot002.png)
