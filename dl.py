import sys
import os
import twist
import requests


ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"

# From: https://stackoverflow.com/a/16696317/3286892
def download_file(url, referer, local_filename=None):
    if not local_filename:
        local_filename = url.split("/")[-1]
    headers = {"user-agent": ua, "referer": referer}
    with requests.get(url, headers=headers, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, "wb") as f:
            for chunk in r.iter_content(chunk_size=8192):
                f.write(chunk)
    return local_filename


if len(sys.argv) >= 2:
    num = 0
    slug = sys.argv[1]
    os.makedirs(slug, exist_ok=True)
    for url in twist.get_anime_sources(slug):
        num += 1
        referer = f"https://twist.moe/a/{slug}/{num}"
        path = os.path.join(slug, f"{num}.mp4")

        print(f"Downloading {url} to {path}")

        download_file(url, referer, path)
